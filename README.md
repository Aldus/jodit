### jodit
This project is the implementation of the [Jodit][2] editor to LEPTON CMS.
See [Jodit][3]

#### Requirements

* [LEPTON CMS][1], current Version

[1]: https://lepton-cms.org "LEPTON CMS"
[2]: https://github.com/xdan/jodit/ "JoDit"
[3]: https://xdsoft.net/jodit/ "jodit"