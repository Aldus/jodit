<?php

/**
 *  @module         Jodit
 *  @copyright      2024-2024 Dietrich Roland Pehlke (Aldus)
 *  @version        see info.php of this module
 *  @authors        Dietrich Roland Pehlke (Aldus)
 *  @license        GNU General Public License for this module, Jodit: MIT license
 *  @license terms  see info.php of this module
 *
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) { 
        include $root.SEC_FILE;   
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure file

function show_wysiwyg_editor($name, $id, $content, $width, $height, $prompt) 
{
    return jodit::show_wysiwyg_editor($name, $id, $content, $width, $height, $prompt);
}
