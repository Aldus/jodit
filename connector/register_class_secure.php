<?php

/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2023 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

file_put_contents(__DIR__."/ERR.TXT", "i am called here".__FILE__);

$files_to_register = [
	'config.php'
];

LEPTON_secure::getInstance()->accessFiles( $files_to_register );

