<?php

/**
 *  @module         jodit
 *  @version        see info.php of this module
 *  @authors        erpe, Dietrich Roland Pehlke (Aldus)
 *  @copyright      2010-2024 erpe, Dietrich Roland Pehlke (Aldus)
 *  @license        GPLv2+ License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */
 
class jodit_settings extends LEPTON_abstract implements LEPTON_wysiwyg_interface
{
	public $default_skin   = "default";
	public $default_height = "250"; // in px
	public $default_width  = "100"; // in %
	public $default_toolbar = "Full";
	// public $default_content_css = "document";
	
	public $skins = [];
	public $content_css = [];
	public $toolbars = [];
	
	public static $instance;
	
    public $content = "[[lorem]]";

	public function initialize() 
	{
	    // @see: https://xdan.github.io/jodit/
	    // @see: https://xdsoft.net/jodit/docs/classes/toolbar_collection.ToolbarCollection.html
		$this->toolbars = [
				
			'Full'	=> [
			        'source', '|','bold', 'strikethrough', 'underline', 'italic', '|', 'ul', 'ol', '|', 'outdent', 'indent',  '|',
                    'font', 'fontsize', 'brush', 'paragraph', '|', 'image', 'video', 'table', 'link', '|',
                    'align', 'undo', 'redo', '|', 'hr', 'eraser', 'copyformat', '|', 'symbol', 'fullsize', 'print', 'about'
                ],

			/**
			 *	Smart toolbar within only first two rows.
			 *
			 */
			'Smart' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | link unlink image mailto | pagelink droplets",
				
			/**
			 *	Simple toolbar within only one row.
			 *
			 */
			'Simple' => "bold italic | alignleft aligncenter alignright alignjustify | link unlink image mailto | pagelink droplets",
				
		];
	}
        
    // interfaces
    //  [1]
    public function getHeight()
    {
       return $this->default_height; 
    }
    //  [2]
    public function getWidth()
    {
        return $this->default_width; 
    }
    //  [3]
    public function getToolbar()
    {
        return $this->toolbars[ $this->default_toolbar ]; 
    }
    //  [4]
    public function getSkin()
    {
        return $this->default_skin ?? ""; 
    }
}	

