<?php

/**
 *  @module         jodit
 *  @version        see info.php of this module
 *  @authors        erpe, Dietrich Roland Pehlke (Aldus)
 *  @copyright      2010-2024 erpe, Dietrich Roland Pehlke (Aldus)
 *  @license        GPLv2+ License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */
 
class jodit_settings_custom extends jodit_settings
{
	
	public static $instance;

    public $content = "This text comes from the <strong>jodit</strong>_settings_<em>custom</em>-class as a property.\n";
	
	public function initialize() 
	{
	    parent::initialize();
	    // @see: https://xdan.github.io/jodit/
	    // @see: https://xdsoft.net/jodit/docs/classes/toolbar_collection.ToolbarCollection.html
		$this->toolbars['Own experimental'] = [
				
			/**
			 *	Simple toolbar within only one row.
			 *
			 */
			 'undo', 'redo', '|', 'bold', 'about', 'image', '|'
				
		];
	}

}	

