<?php

declare(strict_types=1);

/**
 *  @module         Jodit
 *  @copyright      2024-2024 Dietrich Roland Pehlke (Aldus)
 *  @version        see info.php of this module
 *  @authors        Dietrich Roland Pehlke (Aldus)
 *  @license        GNU General Public License for this module, Jodit: MIT license
 *  @license terms  see info.php of this module
 *
 *
 */

class jodit extends LEPTON_abstract
{
    public static $instance;
 
    // protected string $defaultLinkClass = "jodit_lepton_pagelink";

    protected function initialize(): void
    {
        // required by parent
    }

    public function quick(): string
    {
        $__CLASS__ = __CLASS__;
        
        LEPTON_handle::register("random_string");
        
        $id = "editor".random_string();
        $idForPageSelect = "aldus_".random_string();
        $idForCheckbox = "aldus_".random_string();
        $idForDropletSelect = "aldus_".random_string();
        
        $_SESSION['filebrowser'] = "LEPTON";
        $_SESSION['LEPTON_URL'] = LEPTON_URL;
        
        $oTWIG = lib_twig_box::getInstance();
        $oTWIG->registerModule($__CLASS__, $__CLASS__);

        return $oTWIG->render(
            "@".$__CLASS__."/quick.lte",
            [
                'id'            => $id,
                'id_pageselect' => $idForPageSelect,
                'id_titlecheckbox' => $idForCheckbox,
                'width'         => 80,  // in %
                'height'        => 250, // im px
                'content'       => '[[lorem2?set=berthold&offset=1]]',
                'select_page'   => self::buildPageSelect($idForPageSelect),
                'page_usetitle' => self::buildCheckbox($idForCheckbox),
                'language'      => strtolower(LANGUAGE),
                'defaultLinkClass' => "jodit_lepton_pagelink", //$this->defaultLinkClass
                'id_dropletselect' => $idForDropletSelect,
                'select_droplet'   => self::buildDropletSelect($idForDropletSelect)
            ]
        );
    }
    
    protected static function buildPageSelect(string $id=""): string
    {
        // 1
        $str = "<select id=\"".$id."\" class=\"jodit-input\" style=\"max-width: 80%; margin: 16px;\">";
        
        // 2
        LEPTON_handle::register("page_tree");
        
        $pageTree = [];
        page_tree(0, $pageTree);
        
        // 3        
        self::buildOption($pageTree, $str, 0);
        
        // 4
        $str .= "</select>";
        
        return $str;
    }
    
    protected static function buildOption(array $aTerm = [], string &$storage = "", int $deep=0): void
    {
        foreach ($aTerm as $ref)
        {
            $before = str_repeat("- ", $deep);
            $storage .= "<option value=\"".$ref['page_id']."\">".$before.$ref['page_title']."</option>";
            
            if (!empty($ref['subpages']))
            {
                self::buildOption($ref['subpages'], $storage, $deep+1);
            }
        }
    }
    
    protected static function buildCheckbox(string $id=""): string
    {
        $str = "<div style=\"margin: 16px;\"><input type=\"checkbox\" id=\"".$id."\" value=\"2\">";
        $str .= "<label for=\"".$id."\" style=\"margin-left: 12px;\">use page-title</lable></div>";
        
        return $str;
    }
    
    protected static function buildDropletSelect(string $id): string
    {
        $database = LEPTON_database::getInstance();
        
        $aAllDroplets = [];
        $database->execute_query(
            "SELECT `id`, `name`, `description` FROM `".TABLE_PREFIX."mod_droplets` ORDER BY `name`",
            true,
            $aAllDroplets,
            true
        );
    
        $str = "<select id=\"".$id."\" class=\"jodit-input\" style=\"max-width: 80%; margin: 16px;\">";
        foreach ($aAllDroplets as $tempDropplet)
        {
            $str = "<option value=\"".$tempDropplet['name']."\">".$tempDropplet['name']."</option>";
        }
        $str .= "</select>";
        
        return $str;
    }
    
    /**
     * Initialize editor and create an textarea
     *
     * @param string $name              Name of the textarea.
     * @param string $id                Id of the textarea.
     * @param string $content           The content to edit.
     * @param int|string|null $width    The width of the editor, overwrites the wysiwyg-settings!.
     * @param int|string|null $height   The height of the editor, overwrites the wysiwyg-settings!
     * @param bool $prompt              Direct output to the client via echo (true) or returnd as HTML-textarea (false)?
     * @return bool|string              Could be a BOOL or STR (textarea-tags).
     *
     */
    static public function show_wysiwyg_editor(
        string $name,
        string $id,
        string $content,
        int|string|null $width = null,
        int|string|null $height = null,
        bool $prompt=true
    ): bool|string
    {
        $__CLASS__ = __CLASS__;
        
        LEPTON_handle::register("random_string");
        
        $idForPageSelect = "aldus_".random_string();
        $idForCheckbox = "aldus_".random_string();
        $idForCheckbox = "aldus_".random_string();
        $idForDropletSelect = "aldus_".random_string();

        $_SESSION['filebrowser'] = "LEPTON";
        $_SESSION['LEPTON_URL'] = LEPTON_URL;
        
        /**
         *
         */
        $bWysiwygClassOk = false;
        $oSettings = null;
        $toolbar = "";
        if (class_exists('wysiwyg_settings',true))
        {
            // [1.1.0] Geht the instance
            $oSettings = wysiwyg_settings::getInstance();
            
            // [1.1.1] Check for entries for this editor
            if( !empty($oSettings->editor_settings) )
            {
                // [1.1.2] Class exists and there are entries for this editor
                $bWysiwygClassOk = true;
                $toolbar = implode("','", $oSettings->getToolbar());
                
                if (is_null($width))
                {
                    $width = $oSettings->getWidth();
                }
                
                if (is_null($height))
                {
                    $height = $oSettings->getHeight();
                }
            }
        }
         
        $oTWIG = lib_twig_box::getInstance();
        $oTWIG->registerModule($__CLASS__, $__CLASS__);

        $initSource = "";
        if (!defined("JODIT_INIT"))
        {
            // first run
            $initSource = $oTWIG->render(
                "@".$__CLASS__."/basic.lte",
                []
            );
        }
        
        $editor = $oTWIG->render(
            "@".$__CLASS__."/editor.lte",
            [
                'id'            => $id,
                'id_pageselect' => $idForPageSelect,
                'id_titlecheckbox' => $idForCheckbox,
                'width'         => $width,  // in %
                'height'        => $height, // im px
                'content'       => $content,
                'select_page'   => self::buildPageSelect($idForPageSelect),
                'page_usetitle' => self::buildCheckbox($idForCheckbox),
                'language'      => strtolower(LANGUAGE),
                'defaultLinkClass' => "jodit_lepton_pagelink", // $this->defaultLinkClass
                'id_dropletselect' => $idForDropletSelect,
                'select_droplet'   => self::buildDropletSelect($idForDropletSelect),
                'toolbar'       => $toolbar,
                'JODIT'         => self::getInstance()
            ]
        );
        
        if ($prompt == true)
        {
            echo $initSource.$editor;
            return "";
        }
        
        return $initSource.$editor;
        
    }
}